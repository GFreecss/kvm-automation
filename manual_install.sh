#!/bin/bash
set -x

virt-install \
	--connect qemu+ssh://orbitadmin@10.209.12.222/system \
	--name ubu-vm-1 \
	--vcpus=2 \
	--memory=2048 \
	--location="http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/" \
	--graphics vnc \
	--network network=host-bridge \
	--disk size=12,format=qcow2


	# --console pty,target.type=serial \
