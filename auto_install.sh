#!/bin/bash

set -x

virt-install \
	--name ubu-vm-1 \
	--vcpus=4 \
	--ram=4096 \
	--location="http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/" \
	--initrd-inject=preseed.cfg \
	--extra-args="auto console=ttyS0, 115200n8 serial" \
	--graphics none \
	--console pty,target.type=serial \
	--extra-args="ks=file:/preseed.cfg" \
	--network network=host-bridge \
	--disk size=50,format=qcow2 \
